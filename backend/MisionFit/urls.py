"""MisionFit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from MisionFitApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('CrearUsuario/', views.crear_usuario_vista.CrearUsuarioVista.as_view()),
    path('Usuario/<int:pk>', views.detalles_usuario_vista.DetallesUsuarioVista.as_view()),
    path('HorariosSede/',  views.horarios_sede_vista.HorariosSedeVista.as_view({'get': 'list'})),
    path('HorariosFecha/',  views.horarios_fecha_vista.HorariosFechaVista.as_view({'get': 'list'})),
    path('Sedes/', views.sede_vista.SedesVista.as_view({'get': 'list'})),
    path('Horarios/', views.horarios_vista.HorariosVista.as_view({'get': 'list'})),
    path('Reservas/Usuario/<int:pk>/', views.detalles_reservas_vista.DetallesReservasVista.as_view()),
    path('CrearBorrarReserva/', views.crear_borrar_reserva_vista.CrearYBorrarReservaVista.as_view())
]
