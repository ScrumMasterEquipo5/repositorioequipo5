from django.contrib import admin
from .models.usuarios import Usuarios
from .models.horarios import Horarios
from .models.horarios_por_sede import HorariosPorSede
from .models.horarios_por_fecha import HorariosPorFecha
from .models.reservas_por_usuario import ReservasPorUsuario
from .models.sedes import Sedes
from .models.dias_mes import DiasMes

admin.site.register(Usuarios)
admin.site.register(Horarios)
admin.site.register(Sedes)
admin.site.register(DiasMes)
admin.site.register(HorariosPorSede)
admin.site.register(HorariosPorFecha)
admin.site.register(ReservasPorUsuario)