from rest_framework import serializers
from MisionFitApp.models.usuarios import Usuarios
from MisionFitApp.models.informacion_usuarios import InformacionUsuarios
from MisionFitApp.serializers.info_usuariosSerializer import InfoUsuariosSerializer
class UsuariosSerializer(serializers.ModelSerializer):
    informacion_usuarios = InfoUsuariosSerializer()
    class Meta:
        model = Usuarios
        fields = ['id', 'idUsuarios', 'password', 'informacion_usuarios']
    
    def create(self, validated_data):
        informacion_usuariosData = validated_data.pop('informacion_usuarios')
        usuarioInstance = Usuarios.objects.create(**validated_data)
        InformacionUsuarios.objects.create( usuario = usuarioInstance, **informacion_usuariosData )
        return usuarioInstance
    
    def to_representation(self, obj):
        usuario = Usuarios.objects.get(id = obj.id)
        informacion_usuarios = InformacionUsuarios.objects.get(usuario = obj.id)
        return {
            'id': usuario.id,
            'idUsuarios': usuario.idUsuarios,
            'informacion_usuarios' : {
                'id': informacion_usuarios.id,
                'tipoDocumento': informacion_usuarios.tipoDocumento,
                'nombres': informacion_usuarios.nombres,
                'apellidos': informacion_usuarios.apellidos,
                'fechaNacimiento': informacion_usuarios.fechaNacimiento,
                'direccion': informacion_usuarios.direccion,
                'telefono1': informacion_usuarios.telefono1,
                'telefono2': informacion_usuarios.telefono2,
                'correo': informacion_usuarios.correo,
                }
        }