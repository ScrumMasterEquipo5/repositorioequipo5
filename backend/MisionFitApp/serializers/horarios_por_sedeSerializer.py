from rest_framework import serializers   
from MisionFitApp.models.horarios_por_sede import HorariosPorSede
from MisionFitApp.models.horarios import Horarios
from MisionFitApp.models.sedes import Sedes
from MisionFitApp.serializers.horariosSerializer import HorariosSerializer
from MisionFitApp.serializers.sedesSerializer import SedesSerializer


class HorariosSedeSerializer(serializers.ModelSerializer):
    idHorarios = HorariosSerializer()
    idSede = SedesSerializer()
    class Meta:
        model = HorariosPorSede
        fields = ['idHorariosSede', 'idHorarios', 'idSede']
