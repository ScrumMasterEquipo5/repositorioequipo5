from MisionFitApp.models.dias_mes import DiasMes
from rest_framework import serializers


class DiasMesSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiasMes
        fields = ['idDiasMes', 'fecha']
