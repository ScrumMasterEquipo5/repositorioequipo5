from rest_framework import serializers
from MisionFitApp.models.horarios import Horarios

from MisionFitApp.models.reservas_por_usuario import ReservasPorUsuario
from MisionFitApp.models.horarios_por_sede import HorariosPorSede
from MisionFitApp.models.dias_mes import DiasMes
from MisionFitApp.models.horarios_por_fecha import HorariosPorFecha
from MisionFitApp.models.sedes import Sedes
from MisionFitApp.models.usuarios import Usuarios

from MisionFitApp.serializers.usuariosSerializer import InfoUsuariosSerializer
from MisionFitApp.serializers.horarios_por_fechaSerializer import HorariosFechaSerializer

import datetime 

class ReservasUsuarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReservasPorUsuario
        fields = ['idReservas', 'fechaRegistro', 'usuario', 'idHorariosFecha']
    

 
    

    def to_representation(self, obj):
        reserva = ReservasPorUsuario.objects.get(idReservas = obj.idReservas)
        usuario = Usuarios.objects.get(id = reserva.usuario.id)
        horarioPorFecha = HorariosPorFecha.objects.get(idHorariosFecha = reserva.idHorariosFecha.idHorariosFecha)
        diasMes = DiasMes.objects.get(idDiasMes = horarioPorFecha.idDiasMes.idDiasMes)
        horariosPorSede = HorariosPorSede.objects.get(idHorariosSede = horarioPorFecha.idHorariosSede.idHorariosSede)
        horario = Horarios.objects.get(idHorarios = horariosPorSede.idHorarios.idHorarios)
        sede = Sedes.objects.get(idSedes = horariosPorSede.idSede.idSedes) 

        return {
            'ID': reserva.idReservas,
            'Fecha de registro': reserva.fechaRegistro,
            'Usuario': {
                "id": usuario.id,
                "DNI": usuario.idUsuarios
            },
            'Tiempo':{
                'Fecha': diasMes.fecha,
                'Hora inicio': horario.horaInicio,
                "Hora fin": horario.horaFin,
            },
            'Sede':{
                'id': sede.idSedes,
                'Nombre': sede.nombre,
                'Direccion': sede.direccion,
                'Telefono': sede.telefono
            },
        }