from MisionFitApp.models.sedes import Sedes
from rest_framework import serializers


class SedesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sedes
        fields = ['idSedes', 'nombre', 'direccion', 'telefono']
