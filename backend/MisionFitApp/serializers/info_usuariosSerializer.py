from MisionFitApp.models.informacion_usuarios import InformacionUsuarios
from rest_framework import serializers

class InfoUsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = InformacionUsuarios
        fields = ['tipoDocumento', 'nombres', 'apellidos', 'fechaNacimiento', 'direccion', 'telefono1', 'telefono2', 'correo']