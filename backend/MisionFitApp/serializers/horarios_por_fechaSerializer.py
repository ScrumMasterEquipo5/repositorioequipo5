from rest_framework import serializers   
from MisionFitApp.models.horarios_por_fecha import HorariosPorFecha
from MisionFitApp.models.dias_mes import DiasMes
from MisionFitApp.models.horarios_por_sede import HorariosPorSede
from MisionFitApp.serializers.dias_mesSerializer import DiasMesSerializer
from MisionFitApp.serializers.horarios_por_sedeSerializer import HorariosSedeSerializer


class HorariosFechaSerializer(serializers.ModelSerializer):
    idDiasMes = DiasMesSerializer()
    idHorariosSede = HorariosSedeSerializer()

    class Meta:
        model = HorariosPorFecha
        fields = ['idHorariosFecha', 'cuposDisponibles','idDiasMes', 'idHorariosSede']
