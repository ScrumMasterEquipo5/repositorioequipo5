from MisionFitApp.models.horarios import Horarios
from rest_framework import serializers


class HorariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horarios
        fields = ['idHorarios', 'horaInicio', 'horaFin']
