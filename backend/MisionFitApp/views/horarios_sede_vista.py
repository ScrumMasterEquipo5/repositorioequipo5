from MisionFitApp.models.horarios_por_sede import HorariosPorSede
from MisionFitApp.serializers.horarios_por_sedeSerializer import HorariosSedeSerializer
from rest_framework import viewsets

class HorariosSedeVista(viewsets.ModelViewSet):
    queryset =  HorariosPorSede.objects.all()
    serializer_class = HorariosSedeSerializer