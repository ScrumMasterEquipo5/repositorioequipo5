from rest_framework import status, views
from rest_framework.response import Response
from MisionFitApp.models.reservas_por_usuario import ReservasPorUsuario
from MisionFitApp.models.horarios_por_sede import HorariosPorSede
from MisionFitApp.models.dias_mes import DiasMes
from MisionFitApp.models.horarios_por_fecha import HorariosPorFecha
from MisionFitApp.models.sedes import Sedes
from MisionFitApp.models.usuarios import Usuarios
from MisionFitApp.serializers.reservas_por_usuarioSerializer import ReservasUsuarioSerializer
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated


class CrearYBorrarReservaVista(views.APIView):

    permission_classes = (IsAuthenticated,)
    
    def post(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm = settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify = False)
        
        usuario = Usuarios.objects.filter(idUsuarios = request.data['dni']).first()

        if valid_data['user_id'] != usuario.id:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        horarioPorSede = HorariosPorSede.objects.filter(idHorarios = request.data['idHorario'], idSede = request.data['idSede']).first()
        diaMes = DiasMes.objects.filter( fecha = request.data['fecha']).first()
        horarioPorFecha = HorariosPorFecha.objects.filter(idHorariosSede = horarioPorSede.idHorariosSede, idDiasMes = diaMes.idDiasMes).first()

        if horarioPorFecha.cuposDisponibles <= 0:
            raise Exception("No hay cupos disponibles")
        
        processedData = {
            "usuario": usuario.id,
            "idHorariosFecha": horarioPorFecha.idHorariosFecha
        }

        serializer = ReservasUsuarioSerializer( data = processedData)
        serializer.is_valid( raise_exception = True )
        serializer.save()

        horarioPorFecha.cuposDisponibles -= 1
        horarioPorFecha.save()
    
        return Response({"Mensaje": "Se creó la reserva exitosamente"}, status = status.HTTP_201_CREATED)
    

    def delete(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm = settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify = False)
        
        usuario = Usuarios.objects.filter(idUsuarios = request.data['dni']).first()

        if valid_data['user_id'] != usuario.id:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status = status.HTTP_401_UNAUTHORIZED)
        
        reserva = ReservasPorUsuario.objects.get(idReservas = request.data['idReservas'])
        horarioPorFecha = HorariosPorFecha.objects.get(idHorariosFecha = reserva.idHorariosFecha.idHorariosFecha)
        reserva.delete()

        horarioPorFecha.cuposDisponibles += 1
        horarioPorFecha.save()
    
        return Response({"Mensaje": "Se borró la reserva exitosamente"}, status = status.HTTP_202_ACCEPTED)