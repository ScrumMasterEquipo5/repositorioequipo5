from MisionFitApp.models.sedes import Sedes
from MisionFitApp.serializers.sedesSerializer import SedesSerializer
from rest_framework import viewsets

class SedesVista(viewsets.ModelViewSet):
    queryset =  Sedes.objects.all()
    serializer_class = SedesSerializer