from django.db.models.query import QuerySet
from MisionFitApp.models.reservas_por_usuario import ReservasPorUsuario
from MisionFitApp.serializers.reservas_por_usuarioSerializer import ReservasUsuarioSerializer
from rest_framework import generics, viewsets
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

class DetallesReservasVista(generics.RetrieveAPIView):
    queryset = ReservasPorUsuario.objects.all()
    serializer_class = ReservasUsuarioSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm = settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify = False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status = status.HTTP_401_UNAUTHORIZED)
        reservasFiltradas = ReservasPorUsuario.objects.filter(usuario = kwargs['pk'])
        serializer = ReservasUsuarioSerializer(reservasFiltradas, many = True)

        return Response(serializer.data, status = status.HTTP_200_OK)
        
    
    