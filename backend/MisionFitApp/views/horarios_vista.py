from MisionFitApp.models.horarios import Horarios
from MisionFitApp.serializers.horariosSerializer import HorariosSerializer
from rest_framework import viewsets

class HorariosVista(viewsets.ModelViewSet):
    queryset =  Horarios.objects.all()
    serializer_class = HorariosSerializer