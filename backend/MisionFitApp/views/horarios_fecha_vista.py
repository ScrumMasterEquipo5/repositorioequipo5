from MisionFitApp.models.horarios_por_fecha import HorariosPorFecha
from MisionFitApp.serializers.horarios_por_fechaSerializer import HorariosFechaSerializer
from rest_framework import viewsets

class HorariosFechaVista(viewsets.ModelViewSet):
    queryset =  HorariosPorFecha.objects.all()
    serializer_class = HorariosFechaSerializer 