from django.db import models
from .horarios_por_sede import HorariosPorSede
from .dias_mes import DiasMes

class HorariosPorFecha(models.Model):
    idHorariosFecha = models.AutoField(primary_key=True)
    idHorariosSede = models.ForeignKey(HorariosPorSede, related_name='horarios_por_fecha', on_delete=models.CASCADE, null=False)
    idDiasMes = models.ForeignKey(DiasMes, related_name='horarios_por_fecha', on_delete=models.CASCADE, null=False)
    cuposDisponibles = models.SmallIntegerField(null=False)