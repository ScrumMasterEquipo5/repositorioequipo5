from django.db import models

class Sedes(models.Model):
    idSedes = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=45, null=False)
    direccion = models.CharField(max_length=45, null=False)
    telefono = models.CharField(max_length=45, null=True)