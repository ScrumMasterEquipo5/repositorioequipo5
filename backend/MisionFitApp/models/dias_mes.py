from django.db import models

class DiasMes(models.Model):
    idDiasMes = models.AutoField(primary_key=True)
    fecha = models.DateField(null=False)