from django.db import models

class Horarios(models.Model):
    idHorarios = models.AutoField(primary_key=True)
    horaInicio = models.TimeField(null=False)
    horaFin = models.TimeField(null=False)