from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, idUsuarios, password = None):
        """
        Creates and saves a user with the given username and password.
        """
        if not idUsuarios:
            raise ValueError('Users must have an username')
        usuarios = self.model(idUsuarios = idUsuarios)
        usuarios.set_password(password)
        usuarios.save(using=self._db)
        return usuarios


    def create_superuser(self, idUsuarios, password):
        '''
        Creates and saves a superuser with the given username and password.
        '''
        usuario = self.create_user(
            idUsuarios = idUsuarios,
            password = password
        )
        usuario.is_admin = True
        usuario.save(using=self._db)
        return usuario


class Usuarios(AbstractBaseUser, PermissionsMixin):
    id = models.AutoField(primary_key = True)
    idUsuarios = models.CharField( unique = True, max_length = 30 )
    password = models.CharField( max_length = 256, null = True)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'idUsuarios'