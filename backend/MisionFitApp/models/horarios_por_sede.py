from django.db import models
from .horarios import Horarios
from .sedes import Sedes

class HorariosPorSede(models.Model):
    idHorariosSede = models.AutoField(primary_key=True)
    idHorarios = models.ForeignKey(Horarios, related_name = 'horarios', on_delete = models.SET_NULL, null=True)
    idSede = models.ForeignKey(Sedes, related_name = 'sedes', on_delete = models.PROTECT, null = False)
