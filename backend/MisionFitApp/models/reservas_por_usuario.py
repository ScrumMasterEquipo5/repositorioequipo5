from django.db import models
from .usuarios import Usuarios
from .horarios_por_fecha import HorariosPorFecha

class ReservasPorUsuario(models.Model):
    idReservas = models.AutoField(primary_key=True)
    usuario = models.ForeignKey(Usuarios, on_delete = models.RESTRICT, null=False)
    idHorariosFecha = models.ForeignKey(HorariosPorFecha, on_delete = models.RESTRICT, null=False)
    fechaRegistro = models.DateTimeField(null=False, auto_now = True)