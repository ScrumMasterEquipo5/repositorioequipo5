from django.db import models
from .usuarios import Usuarios

class InformacionUsuarios(models.Model):
    choices = (('TI', 'TI'),('CC', 'CC'),('CE', 'CE'))

    id = models.AutoField(primary_key = True)
    usuario = models.ForeignKey(Usuarios, related_name = 'informacion_usuarios', on_delete=models.CASCADE)
    tipoDocumento = models.CharField( max_length = 2, null = False, choices = choices )
    nombres = models.CharField( max_length = 50, null = False )
    apellidos = models.CharField( max_length = 50, null = False )
    fechaNacimiento = models.DateField( null = False )
    direccion = models.CharField( max_length = 45, null = False )
    telefono1 = models.CharField( max_length = 45, null = False )
    telefono2 = models.CharField( max_length = 45, null = True )
    correo = models.EmailField( max_length = 45, null = False )