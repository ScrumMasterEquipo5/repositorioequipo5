import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue';
import SignUp from './components/SignUp.vue';
import Home from './components/Home.vue'
import Reservar from './components/Reservar.vue'
import AdministrarReservas from './components/AdministrarReservas.vue'

const routes = [{
  path: '/',
  name: 'root',
  component: LogIn
},
{
  path: '/user/logIn',
  name: "logIn",
  component: LogIn
},
{
  path: '/user/signUp',
  name: "signUp",
  component: SignUp
},
{
  path: '/user/home',
  name: "home",
  component: Home
},
{
  path: '/user/reserve',
  name: 'reserve',
  component: Reservar
},
{
  path: '/user/reservas',
  name: 'adminReservas',
  component: AdministrarReservas
}
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router